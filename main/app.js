function distance(first, second){
	//TODO: implementați funcția
	// TODO: implement the function
  if((first instanceof Array) &&(second instanceof Array))
  {
  
    let array1=[...new Set(first)];
    let array2=[...new Set(second)];
  
    let uniqueArray1 = array1.filter((o) => array2.indexOf(o) === -1);
    let uniqueArray2 = array2.filter((o) => array1.indexOf(o) === -1);
  
    const uniqueArray = uniqueArray1.concat(uniqueArray2);
  
  
    return uniqueArray.length;
  }
  else {
     throw new Error('InvalidType');
  }

}

